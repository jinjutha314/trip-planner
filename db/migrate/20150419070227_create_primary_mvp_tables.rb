class CreatePrimaryMvpTables < ActiveRecord::Migration
  def change
    create_table :flights do |t|
      t.string  :flight_number
      t.integer :creator_id
      t.integer :trip_id
      t.string  :description
      t.datetime  :arrival_date
      t.timestamps
    end

    create_table :shopping_lists do |t|
      t.string  :description
      t.string  :url
      t.string  :trip_id
      t.integer :creator_id
      t.timestamps
    end

    create_table :notes do |t|
      t.string  :description
      t.integer :creator_id
      t.integer :trip_id
      t.timestamps
    end

    create_table :financial_records  do |t|
      t.integer :amount_in_cents
      t.string  :description
      t.integer :trip_id
      t.integer :creator_id
      t.integer :user_id
      t.timestamps
    end

    create_table :lodging do |t|
      t.integer   :creator_id
      t.datetime  :start_date
      t.datetime  :end_date
      t.integer   :trip_id
      t.string    :name
      t.string    :url
      t.timestamps
    end
  end
end
