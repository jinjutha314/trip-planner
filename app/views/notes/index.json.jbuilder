json.set! :notes do
  json.array! @notes do |note|
    json.(note, :id, :created_at, :description)
    json.creator_name note.user.name
  end
end
