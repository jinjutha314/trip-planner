json.set! :flights do
  json.array! @flights do |f|
    json.(f, :id, :created_at, :description, :flight_number)
    json.arrival_date f.arrival_date.strftime("%m/%d/%Y")
    json.creator_name f.user.name
  end
end
