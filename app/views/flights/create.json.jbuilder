json.(@flight, :id, :created_at, :description, :flight_number)
json.arrival_date @flight.arrival_date.strftime("%m/%d/%Y")
json.creator_name @flight.user.name