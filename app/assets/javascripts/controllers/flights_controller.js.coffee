commonApp.controller("FlightsCtrl", ($scope, $window, $routeParams, $resource, FlightsResource) ->

  window.debug = window.debug || {}
  window.debug["flightsCtrl"] = $scope

  _.extend($scope,
    trip_id: angular.element(".trip-info-container").data().id
    flight_info: {}

    loadNotes: () ->
      flights = FlightsResource.get { trip_id: $scope.trip_id }
      , (success) ->
        $scope.flights = success.flights
      , (error) ->
        console.error error

    submitForm: ($event) ->
      $event.preventDefault()
      FlightsResource.create { trip_id: $scope.trip_id }, $scope.flight_info
      , (response) ->
        $scope.flights.unshift(response)
        $scope.flight_info = {}
      , (error) ->
        console.error error
  )

  $scope.loadNotes()
)