commonApp.controller("FinanceCtrl", ($scope, $window, $routeParams, $resource) ->

  window.debug = window.debug || {}
  window.debug["FinanceCtrl"] = $scope


  _.extend($scope,

    loadFinances: () ->
      #TODO: populate with fake data until controller is implemented
      $scope.finances = [
        {
          amount: "300"
          description: "Paid for Booze Cruise"
          creditor: "Justin"
          debtors: ["Alex", "JJ"].join(", ")
        },
        {
          amount: "200"
          description: "Sky diving deal from Groupon"
          creditor: "JJ"
          debtors: ["Alex", "Justin"].join(", ")
        },
      ]
  )

  $scope.loadFinances()
)