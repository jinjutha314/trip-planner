commonApp.controller("NotesCtrl", ['$scope', 'NotesResource', ($scope, NotesResource) ->

  window.debug = window.debug || {}
  window.debug["notesCtrl"] = $scope

  _.extend($scope,
    trip_id: angular.element(".trip-info-container").data().id

    loadNotes: () ->
      notes = NotesResource.get { trip_id: $scope.trip_id }
      , (success) ->
        $scope.notes = success.notes
      , (error) ->
        console.error error

    submitForm: ($event) ->
      $event.preventDefault()
      NotesResource.create { trip_id: $scope.trip_id }, { description: $scope.description }
      , (response) ->
        $scope.notes.unshift(response)
        $scope.description = null
      , (error) ->
        console.error error

      console.log("herp")

  )

  $scope.loadNotes()
])