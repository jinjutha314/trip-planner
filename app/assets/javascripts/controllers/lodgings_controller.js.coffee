commonApp.controller("LodgingCtrl", ($scope, $window, $routeParams, $resource, NotesResource) ->

  window.debug = window.debug || {}
  window.debug["LodgingCtrl"] = $scope


  _.extend($scope,

    loadLodging: () ->
      #TODO: populate with fake data until controller is implemented
      $scope.lodgings = [
        {
          start_date: "06/10/2015",
          end_date: "06/12/2015",
          name: "Happy Hawaiian House"
          url: ""
          creator_name: "Justin"
        },
        {
          start_date: "06/12/2015",
          end_date: "06/15/2015",
          name: "Camping on the beach"
          url: ""
          creator_name: "Alex"
        },
        {
          start_date: "06/15/2015",
          end_date: "06/17/2015",
          name: "Booze cruise"
          url: ""
          creator_name: "JJ"
        }
      ]
  )

  $scope.loadLodging()
)