commonApp.factory "FlightsResource", ($resource) ->
  $resource "", {},
    get:
      method: 'GET'
      url: Routes.trip_flights_path(":trip_id")
      isArray: false
      params:
        trip_id: "@trip_id"
    create:
      method: 'POST'
      url: Routes.trip_flights_path(":trip_id")
      isArray: false
      params:
        trip_id: "@trip_id"