commonApp.factory("NotesResource", ['$resource', ($resource) ->
  $resource "", {},
    get:
      method: 'GET'
      url: Routes.trip_notes_path(":trip_id")
      isArray: false
      params:
        trip_id: "@trip_id"
    create:
      method: 'POST'
      url: Routes.trip_notes_path(":trip_id")
      isArray: false
      params:
        trip_id: "@trip_id"
])