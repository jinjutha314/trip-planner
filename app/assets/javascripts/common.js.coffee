#= require_self
#= require_tree ./controllers
#= require_tree ./resources

window.commonApp = angular.module("commonApp", [
  'ngResource',
  'ngRoute'
])

commonApp.config ["$httpProvider", ($httpProvider) ->
  $httpProvider.defaults.headers.common['X-CSRF-Token'] = angular.element('meta[name=csrf-token]').attr('content')
]
