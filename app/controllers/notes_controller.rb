class NotesController < ApplicationController
  rescue_from CanCan::AccessDenied do |exception|  # TODO: turn this into a shared module
    respond_to do |format|
      format.json { render :json => 'Access Denied', status: 403 }
    end
  end

  load_and_authorize_resource :trip

  def index
    @notes = @trip.notes
  end

  def create
    @note = @trip.notes.create(permitted_params.merge({creator_id: current_user.id}))
  end

  def destroy

  end

  private

  def permitted_params
    params.permit(:description)
  end
end