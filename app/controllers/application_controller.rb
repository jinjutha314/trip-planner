class ApplicationController < ActionController::Base
  before_action :filter_devise_permitted_params, if: :devise_controller?
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private

  def filter_devise_permitted_params
    registration_params = [:name, :email, :password, :password_confirmation]
    devise_parameter_sanitizer.for(:sign_up) {
        |u| u.permit(registration_params)
    }
  end

  def after_sign_out_path_for(resource_or_scope)
    new_user_session_path()
  end

  def after_sign_in_path_for(resource_or_scope)
    trips_path()
  end
end
