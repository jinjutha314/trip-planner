class FlightsController < ApplicationController
  before_filter :load_trip

  def index
    @flights = @trip.flights
  end

  def create
    params[:arrival_date] = Date.strptime(params[:arrival_date], '%m/%d/%Y')
    @flight = @trip.flights.create(permitted_params.merge({creator_id: current_user.id}))
  end

  def destroy

  end

  private

  def load_trip
    @trip = Trip.find(params[:trip_id])
  end

  def permitted_params
    params.permit(:flight_number, :arrival_date, :description)
  end
end