class Ability
  include CanCan::Ability

  # Very simple authorization for the app. Add more for other models later.
  # See specs at "spec/lib/ability_spec.rb"
  def initialize(user)
    if user.present?
      can :manage, Trip do |trip|
        trip.accessible_by?(user)
      end

      can :manage, Note do |note|
        note.trip.accessible_by?(user)
      end
    else
      can :read, Trip, privacy: Trip::PRIVACY_TYPES[:PUBLIC]

      can :read, Note do |note|
        note.trip.public?
      end
    end
  end
end