class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :name, presence: true

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email,
            presence: {message: "Email address cannot be blank." },
            format: { with: VALID_EMAIL_REGEX, message: "Email address is not valid."},
            uniqueness: {case_sensitive: false, message: "Email address already exist."}

  has_many :user_trips
  has_many :trips, through: :user_trips
end