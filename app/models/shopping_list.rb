class ShoppingList < ActiveRecord::Base
  validates :description, :trip_id, :creator_id, presence: true

  belongs_to :trip
  belongs_to :user, foreign_key: :creator_id
end