class Lodging < ActiveRecord::Base
  validates :start_date, :name, :end_date, :trip_id, presence: true

  belongs_to :trip
  belongs_to :user, foreign_key: :creator_id
end