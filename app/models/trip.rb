class Trip < ActiveRecord::Base
  PRIVACY_TYPES = {
    PUBLIC: 1,
    PRIVATE: 2
  }

  belongs_to :creator, class_name: "User"
  has_many :financial_records
  has_many :user_trips
  has_many :notes
  has_many :flights
  has_many :users, through: :user_trips

  validates :name, presence: true
  validates :privacy, inclusion: { in: PRIVACY_TYPES.values }
  validate :ensure_start_date_earlier_than_end_date
  validates_presence_of :creator

  def public?
    self.privacy == PRIVACY_TYPES[:PUBLIC]
  end

  def accessible_by?(user)
    self.users.include?(user)
  end

  private

  def ensure_start_date_earlier_than_end_date
    errors.add(:start_date, "Start date must come before the end date") unless (self.start_date < self.end_date)
  end
end