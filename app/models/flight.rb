class Flight < ActiveRecord::Base
  validates :flight_number, :creator_id, :trip_id, :description, :arrival_date, presence: true

  belongs_to :user, foreign_key: :creator_id
  belongs_to :trip
end