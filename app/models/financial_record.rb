class FinancialRecord < ActiveRecord::Base
  belongs_to :trip
  validates_presence_of :trip, :user_id, :creator_id, :description

end