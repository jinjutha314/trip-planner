class Note < ActiveRecord::Base
  belongs_to :trip
  belongs_to :user, foreign_key: :creator_id
end