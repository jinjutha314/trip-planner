require "rails_helper"
require "cancan/matchers"

describe "CanCan::Ability" do
  describe "signed in user who is a collaborator of the trip" do
    let(:user) { create(:user) }
    let(:ability) { Ability.new(user) }

    before do
      trip.users << user
    end

    shared_examples_for "CanCan::Ability collaboration should work" do
      it "should allow user to manage the trip" do
        assert_equal(true, ability.can?(:manage, trip))
      end

      it "should allow user to create notes for the trip" do
        assert_equal(true, ability.can?(:create, Note.new(trip_id: trip.id)))
      end

      it "should not allow user to create notes for trip they don't belong to" do
        assert_equal(false, ability.can?(:create, Note.new(trip_id: create(:trip).id)))
      end
    end

    context "private trip" do
      let(:trip)  { create(:trip, :private) }

      it_should_behave_like "CanCan::Ability collaboration should work"
    end

    context "public trip" do
      let(:trip) { create(:trip) }

      it_should_behave_like "CanCan::Ability collaboration should work"
    end

  end

  describe "not signed in user" do
    let(:ability) { Ability.new(nil) }
    let(:public_trip) { create(:trip) }
    let(:private_trip) { create(:trip, :private) }
    let(:note_public) { create(:note, trip: public_trip) }
    let(:note_private) { create(:note, trip: private_trip) }

    context "private trip" do
      it "should not be able to read private trip" do
        assert_equal(false, ability.can?(:read, private_trip))
      end

      it "should not be able to read notes from the trip" do
        assert_equal(false, ability.can?(:read, note_private))
      end

      it "should not be able to create a note for the trip" do
        assert_equal(false, ability.can?(:create, Note.new(trip_id: private_trip.id)))
      end
    end

    context "public trip" do
      it "should be able to read public trip" do
        assert_equal(true, ability.can?(:read, public_trip))
      end

      it "should be able to read notes from a public trip" do
        assert_equal(true, ability.can?(:read, note_public))
      end

      it "should not be able to create a note to a public trip" do
        assert_equal(false, ability.can?(:create, Note.new(trip_id: public_trip.id)))
      end
    end
  end
end