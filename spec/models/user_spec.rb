require "rails_helper"

describe User do
  it "should have a valid factory" do
    expect(build(:user)).to be_valid
  end

  it "should be invalid without name" do
    expect(build(:user, name: "")).to be_invalid
    expect(build(:user, name: nil)).to be_invalid
  end

  it "should be invalid without a valid email address" do
    expect(build(:user, email: "")).to be_invalid
    expect(build(:user, email: nil)).to be_invalid
    expect(build(:user, email: "woof")).to be_invalid
    expect(build(:user, email: "hello-*-@hello.com")).to be_invalid
  end

  it "should create a new user with a valid name and email" do
    expect {
      create(:user, name: "Tassadar", email: "protoss@aiur.com")
    }.to change { User.count }.by(1)

  end
end