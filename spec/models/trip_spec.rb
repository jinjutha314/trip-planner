require "rails_helper"

describe Trip do
  it "should have a valid factory" do
    expect(build(:trip)).to be_valid
    expect(build(:trip, :private)).to be_valid
  end

  context "validations" do
    it "should not be valid without a creator" do
      expect(build(:trip, creator: nil)).to be_invalid
    end

    it "should not be valid if privacy type is not included in the list" do
      expect(build(:trip, privacy: 3)).to be_invalid
    end

    it "should not be valid if the start date is later than the end date" do
      expect {
        trip = build(:trip, start_date: 3.weeks.from_now, end_date: 1.days.from_now)
        trip.save
        expect(trip.errors.messages[:start_date]).to include("Start date must come before the end date")
      }.to_not change { Trip.count }
    end
  end
end