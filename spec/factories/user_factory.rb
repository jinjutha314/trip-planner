FactoryGirl.define do
  factory :user, aliases: [:creator] do
    name "Zeratul"
    sequence(:email) { |n| "meowmix#{n}@meow.com" }
    password "password"
  end
end