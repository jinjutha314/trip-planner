FactoryGirl.define do
  factory :note do
    description "People who bring alcohol must consume the amount they brought."
    user
    trip
  end
end