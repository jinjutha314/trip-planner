FactoryGirl.define do
  factory :trip do
    name      " Party in San Francisco"
    creator
    privacy     Trip::PRIVACY_TYPES[:PUBLIC]
    start_date  2.weeks.from_now
    end_date    3.weeks.from_now

    trait :private do
      privacy  Trip::PRIVACY_TYPES[:PRIVATE]
    end
  end
end