require "rails_helper"

describe NotesController do
  let(:trip) { create(:trip, :private) }
  let(:user) { create(:user) }
  let!(:note) { create(:note, trip_id: trip.id, creator_id: user.id) }
  let(:another_user) { create(:user) }

  before do
    trip.users << user
  end

  describe "routing", type: :routing do
    it "should have a valid index route" do
      expect(get: trip_notes_path(trip.to_param)).to route_to controller: "notes", action: "index", trip_id: trip.to_param, format: :json
    end

    it "should have a valid create route" do
      expect(post: trip_notes_path(trip.to_param)).to route_to controller: "notes", action: "create", trip_id: trip.to_param, format: :json
    end

    it "should have a valid delete route" do
      expect(delete: trip_note_path(trip.to_param, note.to_param)).to route_to controller: "notes", action: "destroy", trip_id: trip.to_param, id: note.to_param, format: :json
    end
  end

describe "POST create" do
  def do_request(options={})
    post :create, options.reverse_merge({ trip_id: trip.id, format: "json"})
  end

  context "authorized user" do
    before do
      sign_in user
    end

    it "should create a new note" do
      expect {
        do_request(description: "meow party")
        expect(response).to be_success
      }.to change { Note.count }.by (1)
      expect(Note.last.description).to eq("meow party")
      expect(Note.last.trip_id).to be(trip.id)
    end
  end

  context "unauthorized user" do
    before do
      sign_in another_user
    end

    it "should not create a new note" do
      expect {
        do_request(description: "meow mix mac meow")
        expect(response).to be_forbidden
      }.to_not change { Note.count }
    end
  end
end

  describe "GET index" do
    let(:another_note) { create(:note) }

    def do_request(options={})
      get :index, options.reverse_merge({ trip_id: trip.id, format: "json"})
    end

    context "authorized user" do
      before do
        sign_in user
      end

      it "should work" do
        do_request
        expect(response).to be_success
      end
    end

    context "unauthorized user" do
      before do
        sign_in another_user
      end

      it "should be forbidden" do
        do_request
        expect(response).to be_forbidden
      end
    end
  end
end